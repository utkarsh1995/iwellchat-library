package com.iw.iwellchatlibrary.dataModel

data class Chat(val viewType: Int, val textMessage: String, val profileImage: Int, val messageDate : String, val messageTime : String)
