package com.iw.iwellchatlibrary

import android.content.Context
import android.content.Intent
import android.widget.Toast

object ChatUtils {

    //Launch Anonymous user chat
    @JvmStatic
    fun launchAnonymousUserChat(c: Context) {
        c.startActivity(Intent(c, AnonymousFormActivity::class.java))
    }

    @JvmStatic
    //Launch Known user chat
    fun launchKnownUserUserChat(c: Context) {
        c.startActivity(Intent(c, ChatThreadStatusActivity::class.java))
    }
}