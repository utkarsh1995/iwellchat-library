package com.iw.iwellchatlibrary

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.iw.iwellchatlibrary.databinding.ActivityAnonymousFormBinding
import com.iw.iwellchatlibrary.databinding.ContentAnonymousFormBinding

class AnonymousFormActivity : AppCompatActivity() {

    private lateinit var anonymousFormBinding: ActivityAnonymousFormBinding
    private lateinit var contentFormBinding: ContentAnonymousFormBinding
    private lateinit var initiateButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        anonymousFormBinding = ActivityAnonymousFormBinding.inflate(layoutInflater)
        setContentView(anonymousFormBinding.root)

        contentFormBinding = ContentAnonymousFormBinding.inflate(layoutInflater)

        anonymousFormBinding.include.anonymousFormBtnInitiateChat.setOnClickListener {
//            Toast.makeText(this, "Button Clicked", Toast.LENGTH_LONG).show()
//            Log.e("button", "clicked")
            ChatUtils.launchKnownUserUserChat(this)
        }

    }
}