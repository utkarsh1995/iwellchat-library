package com.iw.iwellchatlibrary

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.iw.iwellchatlibrary.adapter.ChatThreadAdapter
import com.iw.iwellchatlibrary.dataModel.Chat
import com.iw.iwellchatlibrary.databinding.ActivityChatThreadStatusBinding

class ChatThreadStatusActivity : AppCompatActivity() {
    private lateinit var chatThreadBinding: ActivityChatThreadStatusBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chatThreadBinding = ActivityChatThreadStatusBinding.inflate(layoutInflater)
        setContentView(chatThreadBinding.root)

        val userList = ArrayList<Chat>()
        userList.add(
            Chat(
                ChatThreadAdapter.BROKER_USER_TYPE,
                "I have forgot nominee name, which person added at the time of registration, please provide my kyc detail with nominee ",
                R.drawable.ic_launcher_foreground,
                "16-02-2021",
                "03:40 pm"
            )
        )
        userList.add(
            Chat(
                ChatThreadAdapter.CLIENT_USER_TYPE,
                "Hello customer\nAs we can check, you have already registered your Spouse as nominee",
                R.drawable.ic_launcher_foreground,
                "16-02-2021",
                "03:40 pm"
            )
        )

        userList.add(
            Chat(
                ChatThreadAdapter.BROKER_USER_TYPE,
                "I have forgot nominee name, which person added at the time of registration, please provide my kyc detail with nominee ",
                R.drawable.ic_launcher_foreground,
                "16-02-2021",
                "03:40 pm"
            )
        )
        userList.add(
            Chat(
                ChatThreadAdapter.CLIENT_USER_TYPE,
                "Hello customer\nAs we can check, you have already registered your Spouse as nominee",
                R.drawable.ic_launcher_foreground,
                "16-02-2021",
                "03:40 pm"
            )
        )

        userList.add(
            Chat(
                ChatThreadAdapter.BROKER_USER_TYPE,
                "I have forgot nominee name, which person added at the time of registration, please provide my kyc detail with nominee ",
                R.drawable.ic_launcher_foreground,
                "16-02-2021",
                "03:40 pm"
            )
        )
        userList.add(
            Chat(
                ChatThreadAdapter.CLIENT_USER_TYPE,
                "Hello customer\nAs we can check, you have already registered your Spouse as nominee",
                R.drawable.ic_launcher_foreground,
                "16-02-2021",
                "03:40 pm"
            )
        )

        val adapter = ChatThreadAdapter(this, userList)
        val recyclerView = chatThreadBinding.chatThreadRvChat
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

    }
}
