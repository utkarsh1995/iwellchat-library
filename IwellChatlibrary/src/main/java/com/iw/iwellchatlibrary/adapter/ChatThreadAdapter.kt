package com.iw.iwellchatlibrary.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.iw.iwellchatlibrary.R
import com.iw.iwellchatlibrary.dataModel.Chat

class ChatThreadAdapter(private val context: Context, var list: ArrayList<Chat>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val CLIENT_USER_TYPE = 1
        const val BROKER_USER_TYPE = 2
    }

    private inner class ClientUserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var clientMessage: TextView = itemView.findViewById(R.id.client_tv_message)
        var clientProfile: ImageView = itemView.findViewById(R.id.client_iv_profile)
        var clientDate: TextView = itemView.findViewById(R.id.client_tv_date)
        var clientTime: TextView = itemView.findViewById(R.id.client_tv_time)

        fun bind(position: Int) {
            val chatDataModel = list[position]
            clientMessage.text = chatDataModel.textMessage
            clientProfile.setImageResource(chatDataModel.profileImage)
            clientDate.text = chatDataModel.messageDate
            clientTime.text = chatDataModel.messageTime
        }
    }

    private inner class BrokerUserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var brokerMessage: TextView = itemView.findViewById(R.id.broker_tv_message)
        var brokerProfile: ImageView = itemView.findViewById(R.id.broker_iv_profile)
        var brokerDate: TextView = itemView.findViewById(R.id.broker_tv_date)
        var brokerTime: TextView = itemView.findViewById(R.id.broker_tv_time)

        fun bind(position: Int) {
            val chatDataModel = list[position]

            brokerMessage.text = chatDataModel.textMessage
            brokerProfile.setImageResource(chatDataModel.profileImage)
            brokerDate.text = chatDataModel.messageDate
            brokerTime.text = chatDataModel.messageTime
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == CLIENT_USER_TYPE) {
            return ClientUserViewHolder(
                LayoutInflater.from(context)
                    .inflate(R.layout.item_row_client_message, parent, false)
            )
        }
        return BrokerUserViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_row_broker_message, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (list[position].viewType == CLIENT_USER_TYPE) {
            (holder as ClientUserViewHolder).bind(position)
        } else {
            (holder as BrokerUserViewHolder).bind(position)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return list[position].viewType
    }
}