package com.iw.iwellchatlibrary.fragment

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.iw.iwellchatlibrary.AnonymousFormActivity
import com.iw.iwellchatlibrary.ChatThreadStatusActivity
import com.iw.iwellchatlibrary.R

class CustomToolbarFragment : Fragment() {
    private lateinit var backImage: ImageView
    private lateinit var toolbarTitle: TextView

    companion object {
        fun newInstance(): CustomToolbarFragment {
            return CustomToolbarFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.custom_toolbar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        backImage = view.findViewById(R.id.toolbar_iv_back)
        toolbarTitle = view.findViewById(R.id.toolbar_tv_title)
        backPressed(activity as AppCompatActivity)
    }

    private fun backPressed(activity: AppCompatActivity) {
        if (activity is AnonymousFormActivity) {
            toolbarTitle.text = getString(R.string.toolbar_title_anonymous_chat_initiate)
            backImage.setOnClickListener(View.OnClickListener {
                Toast.makeText(activity, "called from Anonymous Form!", Toast.LENGTH_SHORT).show()
            })
        } else if (activity is ChatThreadStatusActivity) {
            toolbarTitle.text = getString(R.string.toolbar_title_chat_thread)
            backImage.setOnClickListener(View.OnClickListener {
                Toast.makeText(activity, "called from Chat Thread!", Toast.LENGTH_SHORT).show()
            })
        }
    }

}