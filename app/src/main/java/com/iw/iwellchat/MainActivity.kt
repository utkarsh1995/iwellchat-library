package com.iw.iwellchat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.iw.iwellchat.databinding.ActivityMainBinding
import com.iw.iwellchatlibrary.ChatUtils


class MainActivity : AppCompatActivity() {
    private lateinit var mainBinding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)
        ChatUtils.launchAnonymousUserChat(this)
        finish()
    }
}